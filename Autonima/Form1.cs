﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MoreLinq;
namespace Autonima
{
    public partial class Form1 : Form
    {
        private Brain _brain;

        #region "Sensors"
        private ButtonSensorArray _buttonSensor;
        private BlackAndWhiteBitmapSensor _bitmapSensor;
        #endregion

        #region "UI Elements"
        private bool IsMouseDown = false;
        private Point lastMouseLocation;
        private bool BrainActive = false;
        private Stopwatch _neuronGenerationTimer;
        #endregion


        public Form1()
        {
            InitializeComponent();
        }

        private void Init()
        {
            var gridSize = new SizeF(100f, 100f);
            var gridBoundaries = new Size(40, 40);
            var bounds = GridBoundsGenerator.GetBoundsFromGrid(gridSize, gridBoundaries);
            var render = new RenderInfo(bounds, gridSize, gridBoundaries, Vector2.Zero, neuronRender.Size);


            var _projectiles = new ProjectileManager();
            var _projectileFactory = new ProjectileFactory();
            var _synapseFactory = new RandomSynapseFactory();
            var _signalProp = new ConePropogationFunction();
            var neuronCount = 8000;
            var uiController = new NeuronFactoryUIController(worker, neuronCount);
            var _neuronFactory = new RandomNeuronFactory(render, _projectileFactory, _synapseFactory, _signalProp, uiController);
            var _sensors = new SensorManager();
            var _actuators = new ActuatorManager();
            var neuronsAndSynapses = _neuronFactory.GenerateNeurons(neuronCount);
            var _synapses = new SynapseManager(neuronsAndSynapses.Item2, render);

            //Sensor generation
            _buttonSensor = new ButtonSensorArray(neuronsAndSynapses.Item1.Take(10).SelectMany(s => s.Value));
            _sensors.AddSensor(_buttonSensor);


            _bitmapSensor = new BlackAndWhiteBitmapSensor(new Bitmap("simpleGoal.bmp"), neuronsAndSynapses.Item1.Take(10).SelectMany(s => s.Value), 10, 10);
            _sensors.AddSensor(_bitmapSensor);

            //actuator generation
            var actuatorNeuronsLargeSet = MoreEnumerable.RandomSubset(neuronsAndSynapses.Item1.Values, neuronsAndSynapses.Item1.Count).SelectMany(s => s);
            var actuatorNeurons = MoreEnumerable.RandomSubset(actuatorNeuronsLargeSet, 3200).ToList();
            var perceptMotionActuator = new PerceptMotionActuator(actuatorNeurons, LinkPerceptActuatorAndSensor);
            _actuators.AddActuator(perceptMotionActuator);


            _brain = new Brain(neuronsAndSynapses.Item1, _synapses, _sensors, _actuators, _projectiles, render);
            BrainActive = true;
        }

        private void CreateBrainFromStemCells()
        {

        }

        private void BrainTick()
        {
            //scaffolding example of how to link data to sensors
            //external data sources/classes store a ref to the ISensorArray instance and make modifications
            //each braintick as they see fit
            //however, brain is not aware of the semantics of sensors
            _buttonSensor.IsPressed = FireButtonSensors.Checked;
            _brain.Update();
            neuronRender.Invalidate();
            neuronRender.Refresh();
            if(FireButtonSensors.Checked)
            {
                FireButtonSensors.Checked = false;
            }
            offsetLbl.Text = "Render Offset: " + _brain.Render.RenderOffset.ToString();
            ticksAlive.Text = "Ticks alive : " + _brain.Age;
        }

        private void LinkPerceptActuatorAndSensor(Vector2 input)
        {
            _bitmapSensor.ShiftPercept(input);
            perceptLbl.Text = "Percept position " + _bitmapSensor.Percept.ToString();
        }


        private void braintimer_Tick(object sender, EventArgs e)
        {
            if(EnableBrainTick.Checked && BrainActive) BrainTick();
        }

        private void neuronRender_MouseDown(object sender, MouseEventArgs e)
        {
            IsMouseDown = true;
        }

        private void neuronRender_MouseUp(object sender, MouseEventArgs e)
        {
            IsMouseDown = false;
            lastMouseLocation = Point.Empty;
        }

        private void neuronRender_MouseMove(object sender, MouseEventArgs e)
        {  
            if(IsMouseDown && BrainActive)
            {
                if (lastMouseLocation == null || lastMouseLocation == Point.Empty)
                {
                    lastMouseLocation = e.Location;
                }
                var delta = new Vector2(lastMouseLocation.X - e.Location.X, lastMouseLocation.Y - e.Location.Y);
                _brain.Render.RenderOffset += delta;
                lastMouseLocation = e.Location;
                neuronRender.Invalidate();
                neuronRender.Refresh();
            }
            if(BrainActive)
            {
                offsetLbl.Text = "Render Offset: " + _brain.Render.RenderOffset.ToString();
            }
            

        }

        private void btnGenerateNewBrain_Click(object sender, EventArgs e)
        {
            if(!worker.IsBusy)
            {
                _neuronGenerationTimer = new Stopwatch();
                _neuronGenerationTimer.Start();
                worker.RunWorkerAsync();
            }
            

            neuronRender.Invalidate();
            neuronRender.Refresh();
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            Init();
        }

        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var elapsed = _neuronGenerationTimer.ElapsedMilliseconds;
            var timePerNeuron = (1000.0f / (float)elapsed);

            neuronsPerSecondLbl.Text = "Neurons per ms: " + Math.Round(timePerNeuron, 3).ToString();
            pgBarBrainGeneration.Value = e.ProgressPercentage;
            var data = (NeuronFactoryUIProgressReport)e.UserState;
            pgBarCounter.Text = data.NeuronsSoFar.ToString();
            _neuronGenerationTimer.Restart();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            neuronRender.Paint += new System.Windows.Forms.PaintEventHandler(this.neuronRender_Paint);

        }

        private void neuronRender_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            if (BrainActive)
            {
                _brain.DrawBrain(e.Graphics);
            }

        }

        private void DoBrainTick_Click(object sender, EventArgs e)
        {
            if (BrainActive)
            {
                BrainTick();
            }
        }

    }
}
