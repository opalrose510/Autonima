﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    public interface ISynapseFactory
    {
        Synapse MakeSynapse(Neuron parent, Vector2 center, SizeF size, RectangleF bounds);
        ConcurrentStack<Synapse> MakeSynapsePopulation(Neuron parent, int count, Vector2 center, SizeF size, RectangleF bounds);
    }
}
