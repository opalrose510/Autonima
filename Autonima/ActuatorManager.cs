﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    public class ActuatorManager
    {
        public List<IActuator> Actuators { get; private set; } = new List<IActuator>();
        public void AddActuator(IActuator actuator) => Actuators.Add(actuator);

    }
}
