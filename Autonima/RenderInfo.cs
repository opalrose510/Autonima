﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    public class RenderInfo
    {
        public RenderInfo(RectangleF bounds, SizeF sizePerGridSquare, Size gridSize, Vector2 renderOffset, Size renderSize)
        {
            Bounds = bounds;
            SizePerGridSquare = sizePerGridSquare;
            GridSize = gridSize;
            RenderOffset = renderOffset;
            RenderSize = renderSize;
        }

        public RectangleF Bounds { get; private set; }
        public SizeF SizePerGridSquare { get; private set; }
        public Size GridSize { get; private set; }
        public Vector2 RenderOffset { get; set; }
        public Size RenderSize { get; private set; }

        public bool InBounds(Point target)
        {
            var adjustedRender = new RectangleF(-RenderOffset.X, -RenderOffset.Y, RenderSize.Width, RenderSize.Height);
            return adjustedRender.Contains(target);
        }
        /// <summary>
        /// Checks if the tile to the bottom right or the current tile is in the render
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public bool IsCellInBounds(Point target)
        {
            var screenPt = new Point((target.X) * (int)SizePerGridSquare.Width, (target.Y) * (int)SizePerGridSquare.Height);
            var shiftedScreenPt = new Point((target.X + 1)* (int)SizePerGridSquare.Width, (target.Y + 1) * (int)SizePerGridSquare.Height);
            return InBounds(screenPt) || InBounds(shiftedScreenPt);
        }
    }
}
