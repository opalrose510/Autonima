﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.Drawing;
using System.Collections.Concurrent;

namespace Autonima
{
    public class Projectile : Entity
    {
        public Vector2 Velocity { get; set; }
        public Vector2 Target { get; private set; }
        public Guid Id { get; private set; }
        public float RemainingNeurotransmitterAmount { get; private set; }
        public float InitialNeurotransmitterAmount { get; private set; }
        public float TicksUntilDeath { get; private set; }
        public bool IsSourceSignal { get; private set; }
        public Projectile(Vector2 location, Vector2 velocity, Vector2 target, Guid id, float initialNeurotransmitterAmount, bool isSourceSignal) : base(location)
        {
            Velocity = velocity;
            Target = target;
            Id = id;
            InitialNeurotransmitterAmount = initialNeurotransmitterAmount;
            RemainingNeurotransmitterAmount = InitialNeurotransmitterAmount;
            var distance = Vector2.Distance(Target, Location);
            TicksUntilDeath = distance / Velocity.Length();
            IsSourceSignal = isSourceSignal;
        }

        public void Draw(Graphics g, RenderInfo render) {

            var pointLoc = new Point((int)Location.X, (int)Location.Y);
            
            if (render.InBounds(pointLoc))
            {
                var brushColor = IsSourceSignal ? Brushes.Red : Brushes.Green;
                //we do this so we can differentiate visually external and internal signals
                g.FillEllipse(brushColor, Location.X + (int)render.RenderOffset.X, Location.Y + (int)render.RenderOffset.Y, 3, 3); //arbitrary size for now
            }

        }

        /// <summary>
        /// Returns true if the projectile should  be removed, to handle GC
        /// </summary>
        /// <returns></returns>
        public bool Update(RenderInfo render, ConcurrentDictionary<Point, ConcurrentStack<Synapse>> synapses)
        {
            if(TicksUntilDeath >= 1)
            {
                Location = Location + Velocity;
                TicksUntilDeath--;
            } else
            {
                Location = Location + Velocity * TicksUntilDeath;
                TicksUntilDeath = 0;
            }

            if(TicksUntilDeath <= 0)
            {
                var gridSquare = GetGridLocation(render.SizePerGridSquare);
                if (synapses.TryGetValue(gridSquare, out var targetGridSquare))
                {
                    var synapsesInSquare = synapses[gridSquare];

                    foreach (var s in synapsesInSquare)
                    {
                       
                        s.Parent.AxonActivationCondition.Fire(new SynapseActivationAction(s.Weight, s.Location));
                    }
                }
                return true; //indicate that the projectile should be removed
            }

            return !render.Bounds.Contains(Location.X, Location.Y);
        }
    }
}
