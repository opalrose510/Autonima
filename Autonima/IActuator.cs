﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    public interface IActuator
    {
        void Actuate(Neuron actuatedNeuron);
        List<Neuron> GetNeuronsToActivate();
    }
}
