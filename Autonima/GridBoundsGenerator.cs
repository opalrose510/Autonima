﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    public class GridBoundsGenerator
    {
        public static RectangleF GetBoundsFromGrid(SizeF sizePerSquare, Size gridDimensions)
        {
            return new RectangleF(PointF.Empty, new SizeF(sizePerSquare.Width * (float)gridDimensions.Width, sizePerSquare.Height * (float)gridDimensions.Height));
        }
    }
}
