﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    public class SynapseManager
    {
        public ConcurrentDictionary<Point, ConcurrentStack<Synapse>> Synapses { get; private set; } = new ConcurrentDictionary<Point, ConcurrentStack<Synapse>>();

        public SynapseManager(ConcurrentDictionary<Point, ConcurrentStack<Synapse>> synapses, RenderInfo render)
        {
            Synapses = synapses;
        }

        public void Draw(Graphics g, RenderInfo render)
        {
            foreach(var cellGroup in Synapses.Keys)
            {
                if(render.IsCellInBounds(cellGroup)) {
                    foreach(var s in Synapses[cellGroup])
                    {
                        s.Draw(g, render);
                    }
                }

            }
        }
    }
}
