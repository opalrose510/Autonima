﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    public interface INeuronFactory
    {
        Tuple<ConcurrentDictionary<Point, ConcurrentStack<Neuron>>, ConcurrentDictionary<Point, ConcurrentStack<Synapse>>> GenerateNeurons(int count);
        NeuronFactoryUIController GetUIController();
    }
}
