﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    /// <summary>
    /// This is a simplified sensor that simply fires when told.
    /// </summary>
    public class ButtonSensorArray : ISensorArray
    {
        IEnumerable<Neuron> _nodesToFire;
        public bool IsPressed { get; set; }
        public ButtonSensorArray(IEnumerable<Neuron> nodesToFire)
        {
            _nodesToFire = nodesToFire;
        }
        public List<Neuron> Update(ProjectileManager manager)
        {
            var toUpdate = new List<Neuron>();
            if(IsPressed)
            {
                foreach(var n in _nodesToFire)
                {
                    n.Fire(manager);
                }
                
            }
            return toUpdate;

        }
    }
}
