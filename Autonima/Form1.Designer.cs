﻿namespace Autonima
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.neuronRender = new System.Windows.Forms.PictureBox();
            this.FireButtonSensors = new System.Windows.Forms.CheckBox();
            this.DoBrainTick = new System.Windows.Forms.Button();
            this.braintimer = new System.Windows.Forms.Timer(this.components);
            this.EnableBrainTick = new System.Windows.Forms.CheckBox();
            this.offsetLbl = new System.Windows.Forms.Label();
            this.btnGenerateNewBrain = new System.Windows.Forms.Button();
            this.pgBarBrainGeneration = new System.Windows.Forms.ProgressBar();
            this.pgBarCounter = new System.Windows.Forms.Label();
            this.worker = new System.ComponentModel.BackgroundWorker();
            this.neuronsPerSecondLbl = new System.Windows.Forms.Label();
            this.ticksAlive = new System.Windows.Forms.Label();
            this.tickWorker = new System.ComponentModel.BackgroundWorker();
            this.perceptLbl = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.neuronRender)).BeginInit();
            this.SuspendLayout();
            // 
            // neuronRender
            // 
            this.neuronRender.Location = new System.Drawing.Point(2, 7);
            this.neuronRender.Name = "neuronRender";
            this.neuronRender.Size = new System.Drawing.Size(1680, 1022);
            this.neuronRender.TabIndex = 0;
            this.neuronRender.TabStop = false;
            this.neuronRender.MouseDown += new System.Windows.Forms.MouseEventHandler(this.neuronRender_MouseDown);
            this.neuronRender.MouseMove += new System.Windows.Forms.MouseEventHandler(this.neuronRender_MouseMove);
            this.neuronRender.MouseUp += new System.Windows.Forms.MouseEventHandler(this.neuronRender_MouseUp);
            // 
            // FireButtonSensors
            // 
            this.FireButtonSensors.AutoSize = true;
            this.FireButtonSensors.Location = new System.Drawing.Point(1688, 12);
            this.FireButtonSensors.Name = "FireButtonSensors";
            this.FireButtonSensors.Size = new System.Drawing.Size(113, 17);
            this.FireButtonSensors.TabIndex = 1;
            this.FireButtonSensors.Text = "Fire Button Sensor";
            this.FireButtonSensors.UseVisualStyleBackColor = true;
            // 
            // DoBrainTick
            // 
            this.DoBrainTick.Location = new System.Drawing.Point(1688, 68);
            this.DoBrainTick.Name = "DoBrainTick";
            this.DoBrainTick.Size = new System.Drawing.Size(113, 58);
            this.DoBrainTick.TabIndex = 2;
            this.DoBrainTick.Text = "NextTick";
            this.DoBrainTick.UseVisualStyleBackColor = true;
            this.DoBrainTick.Click += new System.EventHandler(this.DoBrainTick_Click);
            // 
            // braintimer
            // 
            this.braintimer.Enabled = true;
            this.braintimer.Interval = 16;
            this.braintimer.Tick += new System.EventHandler(this.braintimer_Tick);
            // 
            // EnableBrainTick
            // 
            this.EnableBrainTick.AutoSize = true;
            this.EnableBrainTick.Location = new System.Drawing.Point(1688, 133);
            this.EnableBrainTick.Name = "EnableBrainTick";
            this.EnableBrainTick.Size = new System.Drawing.Size(110, 17);
            this.EnableBrainTick.TabIndex = 3;
            this.EnableBrainTick.Text = "Enable Brain Tick";
            this.EnableBrainTick.UseVisualStyleBackColor = true;
            // 
            // offsetLbl
            // 
            this.offsetLbl.AutoSize = true;
            this.offsetLbl.Location = new System.Drawing.Point(1688, 168);
            this.offsetLbl.Name = "offsetLbl";
            this.offsetLbl.Size = new System.Drawing.Size(35, 13);
            this.offsetLbl.TabIndex = 4;
            this.offsetLbl.Text = "label1";
            // 
            // btnGenerateNewBrain
            // 
            this.btnGenerateNewBrain.Location = new System.Drawing.Point(1688, 184);
            this.btnGenerateNewBrain.Name = "btnGenerateNewBrain";
            this.btnGenerateNewBrain.Size = new System.Drawing.Size(113, 62);
            this.btnGenerateNewBrain.TabIndex = 5;
            this.btnGenerateNewBrain.Text = "Generate New Brain";
            this.btnGenerateNewBrain.UseVisualStyleBackColor = true;
            this.btnGenerateNewBrain.Click += new System.EventHandler(this.btnGenerateNewBrain_Click);
            // 
            // pgBarBrainGeneration
            // 
            this.pgBarBrainGeneration.Location = new System.Drawing.Point(1692, 252);
            this.pgBarBrainGeneration.Name = "pgBarBrainGeneration";
            this.pgBarBrainGeneration.Size = new System.Drawing.Size(100, 23);
            this.pgBarBrainGeneration.TabIndex = 6;
            // 
            // pgBarCounter
            // 
            this.pgBarCounter.AutoSize = true;
            this.pgBarCounter.Location = new System.Drawing.Point(1689, 278);
            this.pgBarCounter.Name = "pgBarCounter";
            this.pgBarCounter.Size = new System.Drawing.Size(105, 13);
            this.pgBarCounter.TabIndex = 7;
            this.pgBarCounter.Text = "0 neurons generated";
            // 
            // worker
            // 
            this.worker.WorkerReportsProgress = true;
            this.worker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.worker_DoWork);
            this.worker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.worker_ProgressChanged);
            // 
            // neuronsPerSecondLbl
            // 
            this.neuronsPerSecondLbl.AutoSize = true;
            this.neuronsPerSecondLbl.Location = new System.Drawing.Point(1693, 302);
            this.neuronsPerSecondLbl.Name = "neuronsPerSecondLbl";
            this.neuronsPerSecondLbl.Size = new System.Drawing.Size(29, 13);
            this.neuronsPerSecondLbl.TabIndex = 8;
            this.neuronsPerSecondLbl.Text = "n / s";
            // 
            // ticksAlive
            // 
            this.ticksAlive.AutoSize = true;
            this.ticksAlive.Location = new System.Drawing.Point(1693, 325);
            this.ticksAlive.Name = "ticksAlive";
            this.ticksAlive.Size = new System.Drawing.Size(104, 13);
            this.ticksAlive.TabIndex = 9;
            this.ticksAlive.Text = "ticks alive goes here";
            // 
            // tickWorker
            // 
            this.tickWorker.WorkerReportsProgress = true;
            // 
            // perceptLbl
            // 
            this.perceptLbl.AutoSize = true;
            this.perceptLbl.Location = new System.Drawing.Point(1688, 473);
            this.perceptLbl.Name = "perceptLbl";
            this.perceptLbl.Size = new System.Drawing.Size(60, 13);
            this.perceptLbl.TabIndex = 10;
            this.perceptLbl.Text = "percept loc";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1804, 1041);
            this.Controls.Add(this.perceptLbl);
            this.Controls.Add(this.ticksAlive);
            this.Controls.Add(this.neuronsPerSecondLbl);
            this.Controls.Add(this.pgBarCounter);
            this.Controls.Add(this.pgBarBrainGeneration);
            this.Controls.Add(this.btnGenerateNewBrain);
            this.Controls.Add(this.offsetLbl);
            this.Controls.Add(this.EnableBrainTick);
            this.Controls.Add(this.DoBrainTick);
            this.Controls.Add(this.FireButtonSensors);
            this.Controls.Add(this.neuronRender);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.neuronRender)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox neuronRender;
        private System.Windows.Forms.CheckBox FireButtonSensors;
        private System.Windows.Forms.Button DoBrainTick;
        private System.Windows.Forms.Timer braintimer;
        private System.Windows.Forms.CheckBox EnableBrainTick;
        private System.Windows.Forms.Label offsetLbl;
        private System.Windows.Forms.Button btnGenerateNewBrain;
        private System.Windows.Forms.ProgressBar pgBarBrainGeneration;
        private System.Windows.Forms.Label pgBarCounter;
        private System.ComponentModel.BackgroundWorker worker;
        private System.Windows.Forms.Label neuronsPerSecondLbl;
        private System.Windows.Forms.Label ticksAlive;
        private System.ComponentModel.BackgroundWorker tickWorker;
        private System.Windows.Forms.Label perceptLbl;
    }
}

