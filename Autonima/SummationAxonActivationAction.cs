﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    public class SummationAxonActivationAction : IAxonActivationCondition
    {
        private float sumSoFar { get; set; }
        private readonly float _activationThreshold;

        public SummationAxonActivationAction(float activationThreshold)
        {
            _activationThreshold = activationThreshold;
        }
        public AxonPulse Activate()
        {
            sumSoFar = 0.0f;
            return new AxonPulse(0, 1.0f); //ToDo: stub it out
        }

        /// <summary>
        /// We can't use a list here because the synapses passed in a list may have many different parents, just share the same cell
        /// </summary>
        /// <param name="synapseUpdatedThisTick"></param>
        public void Fire(SynapseActivationAction synapseUpdatedThisTick)
        {
            sumSoFar += synapseUpdatedThisTick.Weight;
        }

        public bool ReadyToActivate()
        {
            if(sumSoFar > _activationThreshold)
            {
                return true;
                //not inline so we can add breakpoints
            }
            return false;
        }

        public void Update()
        {
            //here you'd implement something like gradual decay, empty now
        }
    }
}
