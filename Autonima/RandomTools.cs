﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Autonima
{
    public static class RandomTools
    {
        private static int seed = Environment.TickCount;
        private static ThreadLocal<Random> _random = new ThreadLocal<Random>(() => new Random(Interlocked.Increment(ref seed)));
        public static Vector2 MakeRandomVector(float width, float height)
        {
            return new Vector2((float)NextDouble() * (-width) + width, (float) NextDouble() * (-height) + height);

        }
        public static double NextDouble()
        {
            return _random.Value.NextDouble();
        }
        public static float NextFloat()
        {
            double mantissa = (_random.Value.NextDouble() * 2.0) - 1.0;
            double exponent = Math.Pow(2.0, _random.Value.Next(-126, 128));
            return (float)(mantissa * exponent);
        }
    }
}
