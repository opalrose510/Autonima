﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{

    //THINGS WE KNOW
    //Synapses take 0.1 ms to restart. Average firing rate: 1-10 hz
    //Number of synapses in cortex = 0.15 quadrillion (Pakkenberg et al., 1997; 2003)
    //

    public class Synapse : Entity
    {
        public bool Active { get
            {
                return Weight > 1.0f;
            }
        }
        public Neuron Parent { get; private set; }
        public float Weight { get; set; } = 1.0f; //TEMPORARY
        public bool SelectedForPruning {
            get {
                return Weight < 0;
            }
        }
        public Synapse(Neuron parent, Vector2 location) : base(location)
        {
            Parent = parent;
        }

        public void Draw(Graphics g, RenderInfo render)
        {
            if (render.InBounds(Location.ToPointTruncate()))
            {
                if(Active)
                {
                    var pen = new Pen(Brushes.Red, Weight);
                    var synapseLoc = new Point((int)(Location.X + render.RenderOffset.X), (int)(Location.Y + render.RenderOffset.Y));
                    var neuronLoc = new Point((int)(Parent.Location.X + render.RenderOffset.X), (int)(Parent.Location.Y + (int)render.RenderOffset.Y));
                    g.DrawLine(pen, neuronLoc, synapseLoc);
                }

            }
        }
        /// <summary>
        /// Increases the weight by an arbitrary internal amount; a signal does not have strength, only synapse connections.
        /// </summary>
        public void IncreaseWeight()
        {
            Weight++; //we may update this with an arbitrary update value at a later time
        }
        /// <summary>
        /// Triggers 
        /// </summary>
        public void Decay()
        {
            Weight--;
        }
        public void Update()
        {


        }
    }
}
