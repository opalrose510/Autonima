﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Autonima
{
    public class Neuron : Entity
    {
        public readonly Vector2 Target;
        private float _axonProjectileSpeed;
        private IProjectileFactory _projectileFactory;
        public IAxonActivationCondition AxonActivationCondition { get; private set; }
        public IActuator Actuator { get; set; } = null;
        public bool IsSourceSignal { get; private set; } = false;
        public Neuron(Vector2 location, Vector2 target, IProjectileFactory projectileFactory, float axonProjectileSpeed, IAxonActivationCondition axonActivationCondition) : base(location)
        {
            Target = target;
            _projectileFactory = projectileFactory;
            _axonProjectileSpeed = axonProjectileSpeed;
            AxonActivationCondition = axonActivationCondition;
        }
        public void Fire(ProjectileManager manager)
        {
            if(Actuator != null)
            {
                Actuator.Actuate(this);
            }
            manager.AddProjectile(_projectileFactory.MakeProjectile(Location, Target, _axonProjectileSpeed, isSourceSignal: true));
        }
        public void Draw(Graphics g, RenderInfo render)
        {
            var color = System.Drawing.Brushes.Black;
            g.FillEllipse(color, Location.X + (int)render.RenderOffset.X, Location.Y + (int)render.RenderOffset.Y, 7, 7);
        }
        public void Update(ProjectileManager manager)
        {
            if (IsSourceSignal) return; //don't propogate incoming information; this is an edge
            if(AxonActivationCondition.ReadyToActivate())
            {
                AxonActivationCondition.Activate();
                //eventually we will do stuff with AxonPulse, not yet
                //we use isSourceSignal so we can clearly differentiate what's coming in from the outside of thes system
                manager.AddProjectile(_projectileFactory.MakeProjectile(Location, Target, _axonProjectileSpeed, isSourceSignal: false));
            }
        }
        public void MakeSourceSignal()
        {
            IsSourceSignal = true;
        }
    }
}

